from django.shortcuts import render, redirect
from .models import Schedule
from . import forms

# Create your views here.
def about(request):
    return render(request, 'about.html')

def profile(request):
    return render(request, 'profile.html')
    
def Pengalaman(request):
    return render(request, 'Pengalaman.html')

def homepage(request):
    return render(request, 'tes_web.html')

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'schedule.html', {'schedules': schedules})

def schedule_create(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('homepage:schedule')

    else:
        form = forms.ScheduleForm()
    return render(request, 'schedule_create.html', {'form': form})

def schedule_delete(request):
	Schedule.objects.all().delete()
	return render(request, "schedule.html")

