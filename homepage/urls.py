from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    
    path('', views.homepage, name='homepage'),
    path('about/', views.about, name='about'),
    path('pengalaman/', views.Pengalaman, name='exp'),
    path('profile/', views.profile, name='profile'),
    path('schedule/', views.schedule, name= 'schedule'), 
    path('schedule/create', views.schedule_create,name = 'schedule_create'),
    path('schedule/delete', views.schedule_delete,name = 'schedule_delete'),
]